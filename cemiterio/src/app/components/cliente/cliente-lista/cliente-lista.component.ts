import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/resources/services/alert.service';
import { ClienteService } from 'src/app/resources/services/cliente.service';
import { Cliente } from '../Cliente';

@Component({
  selector: 'app-cliente-lista',
  templateUrl: './cliente-lista.component.html',
  styleUrls: ['./cliente-lista.component.css']
})
export class ClienteListaComponent implements OnInit {

  clients: Cliente[] = [];
  clienteSelecionado: Cliente = null;
  mensagemSucesso: string;
  mensagemErro: string;
  //cliente$: Observable<any>;
  // total: number;
   cliente: Cliente;
  searchForm: FormGroup;

  constructor(
    private service: ClienteService,
    private router: Router,
    private alertService: AlertService
  ) {}

  ngOnInit(): void {
     this.service
       .getCliente()
       .subscribe((response) => (this.clients = response));
    this.searchForm = new FormGroup({
      queryField: new FormControl(null, {
        updateOn: "change",
        validators: [Validators.required],
      }),
    });
  }

  novoCadastro() {
    this.router.navigate(["/cliente-form"]);
  }
  preparaDelecao(cliente: Cliente) {
    this.clienteSelecionado = cliente;
  }
  onSearch() {
    // if (this.searchForm.valid) {
    //   this.service
    //     .getClienteByName(this.searchForm.value.queryField)
    //     .subscribe((response) => {
    //       this.clients = response;
    //     });
    // }
  }
  deletarCliente() {
     this.service.deletar(this.clienteSelecionado).subscribe(
       (response) => {
         this.alertService.success(
           "Maravilha",
           "Você optou por excluir esse cliente!"
         );
         this.ngOnInit();
       },
       (errorResponse) => {
         this.alertService.error(errorResponse.error.message);
      }
     );
  }

}
